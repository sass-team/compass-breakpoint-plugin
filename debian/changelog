compass-breakpoint-plugin (3.0.0-1) unstable; urgency=medium

  [ upstream ]
  * new release

  [ Jonas Smedegaard ]
  * update copyright info: use field Reference
    (not License-Reference)
  * declare compliance with Debian Policy 4.6.1
  * drop patch 2001 obsoleted by upstream changes
  * stop handle Ruby code, now dropped upstream;
    stop build-depend on
    gem2deb ruby ruby-sass compass-sassy-maps-plugin

 -- Jonas Smedegaard <dr@jones.dk>  Thu, 25 Aug 2022 17:24:27 +0200

compass-breakpoint-plugin (2.7.1-3) unstable; urgency=medium

  * provide virtual package sass-stylesheets-breakpoint
  * stop depend on ruby ruby-sass
  * relax to build-depend unversioned on ruby-sass:
    needed version satisfied in all supported releases of Debian
  * copyright info: update coverage
  * use debhelper compatibility level 13 (not 9);
    build-depend on debhelper-compat (not debhelper)
  * declare compliance with Debian Policy 4.5.1
  * update source helper script copyright-check

 -- Jonas Smedegaard <dr@jones.dk>  Tue, 02 Mar 2021 12:22:52 +0100

compass-breakpoint-plugin (2.7.1-2) unstable; urgency=medium

  * Simplify rules.
    Stop build-depend on licensecheck cdbs.
  * Stop build-depend on dh-buildinfo.
  * Declare compliance with Debian Policy 4.3.0.
  * Update Vcs-* URLs: Maintenance moved to Salsa.
  * Set Rules-Requires-Root: no.
  * Talk about Sass (not Compass) in long description.
  * Update copyright info:
    + Strip superfluous copyright signs.
    + Use https protocol in Format URL.
    + Drop superfluous alternate Source URL.
    + Extend coverage of packaging.
  * Tighten lintian overrides regarding License-Reference.
  * Update watch file:
    + Simplify regular expressions.
    + Rewrite usage comment.
  * Include sass files below /usr/share/sass.
  * Advertise DEP3 format in patch headers.

 -- Jonas Smedegaard <dr@jones.dk>  Sat, 02 Mar 2019 20:24:50 +0100

compass-breakpoint-plugin (2.7.1-1) unstable; urgency=medium

  [ upstream ]
  * New release.

  [ Jonas Smedegaard ]
  * Modernize CDBS use: Build-depend on licensecheck (not devscripts).
  * Unfuzz patch 2001.

 -- Jonas Smedegaard <dr@jones.dk>  Wed, 14 Dec 2016 10:57:03 +0100

compass-breakpoint-plugin (2.7.0-1) unstable; urgency=medium

  [ upstream ]
  * New release.

  [ Jonas Smedegaard ]
  * Modernize git-buildpackage config:
    + Filter any .git* file.
  * Declare compliance with Debian Policy 3.9.8.
  * Modernize Vcs-Git field to use https URL.
  * Bump debhelper compatibility level to 9.
  * Add lintian override regarding debhelper 9.
  * Update watch file:
    + Modernize to use format 4.
    + Mention gbp in usage hint comment.
  * Update copyright info:
    + Extend copyright of packaging to cover recent years.
  * Unfuzz patch 2001.

 -- Jonas Smedegaard <dr@jones.dk>  Sat, 18 Jun 2016 20:25:38 +0200

compass-breakpoint-plugin (2.6.1-1) unstable; urgency=medium

  [ upstream ]
  * New release(s).

  [ Jonas Smedegaard ]
  * Update upstream URLs to reflect team rename Team-Sass → at-import.
  * Modernize git-buildpackage config: Avoid "git-" prefix.
  * Update copyright info:
    + Use License-Grant and License-Reference fields.
      Thanks to Ben Finney.
  * Add lintian overrides regarding license in License-Reference field.
    See bug#786450.
  * Unfuzz patch 2001.
  * Adapt for renamed upstream README.
  * Install upstream CHANGELOG.

 -- Jonas Smedegaard <dr@jones.dk>  Sat, 20 Jun 2015 21:55:38 -0500

compass-breakpoint-plugin (2.5.0-2) unstable; urgency=medium

  * Update Vcs-Browser URL to use cgit web frontend.
  * Fix use canonical URL in Vcs-Git.
  * Declare compliance with Debian Policy 3.9.6.
  * Stop build-depend explicitly on ruby.

 -- Jonas Smedegaard <dr@jones.dk>  Thu, 23 Oct 2014 02:46:49 +0200

compass-breakpoint-plugin (2.5.0-1) experimental; urgency=medium

  [ upstream ]
  * New release.

  [ Jonas Smedegaard ]
  * Update package relations:
    + Tighten (build-)dependency on ruby-sass.
    + (Build-)depend on compass-sassy-maps-plugin.
    + Stop (build-)depend on ruby-compass.
  * Update patch 2001.
  * Build-depend explicitly on ruby (see bug#758806).

 -- Jonas Smedegaard <dr@jones.dk>  Sat, 23 Aug 2014 14:51:47 +0200

compass-breakpoint-plugin (2.0.6-3) unstable; urgency=medium

  * Fix build-depend (not only depend) on ruby-sass and ruby-compass:
    Needed to resolve gemspec file.
  * Add patch 2001 to point to framework below /usr/share.

 -- Jonas Smedegaard <dr@jones.dk>  Mon, 07 Jul 2014 19:08:24 +0200

compass-breakpoint-plugin (2.0.6-2) unstable; urgency=medium

  * Update Homepage URL.
  * Update Maintainer, Uploaders and Vcs-* fields: Packaging git moved
    to pkg-sass area of Alioth.
  * Bump standards-version to 3.9.5.
  * Extend coverage for myself.

 -- Jonas Smedegaard <dr@jones.dk>  Thu, 01 May 2014 18:12:33 +0200

compass-breakpoint-plugin (2.0.6-1) unstable; urgency=low

  * Initial release.
    Closes: bug#705206.

 -- Jonas Smedegaard <dr@jones.dk>  Fri, 30 Aug 2013 18:52:03 +0200
